﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed;
    public float AngularSpeed;
    public float dist;

    public Rigidbody rb;

    Vector3 movement;

    private void Start()
    {
        //Cursor.visible = false;
    }

    void Update()
    {
    }

    private void FixedUpdate()
    {
        Speed = rb.velocity.magnitude;
        AngularSpeed = rb.angularVelocity.magnitude;

        //Get the Screen positions of the object
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        //Get the Screen position of the mouse
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        //Get the angle between the points
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        //Distance between character and the mouse
        float distance = (Vector2.Distance(positionOnScreen, mouseOnScreen))*2;
        dist = Mathf.Clamp(distance, 0f, 1f);

        //If player presses and while they hold fire1 applies force to rigidbody(player), speed target, acceleration to speed target
        if (Input.GetButton("Fire1"))
        {
            PlayerMovement.ApplyForceToReachVelocity(rb, transform.forward * dist * 60, 1 / dist);
        }

        float acel = dist * 1;

        float x = transform.rotation.eulerAngles.x;
        float z = transform.rotation.eulerAngles.z;

        //Sets player y rotation to angle
        transform.rotation = Quaternion.Euler(new Vector3(x, -angle, z));

        //If player presses and while they hold fire1 applies force to rigidbody(player), speed target, acceleration to speed target
        if (Input.GetButton("Fire1"))
        {
            PlayerMovement.ApplyForceToReachVelocity(rb, transform.forward * dist * 30, 1/dist);
        }

        // reverses gravity and increases drag when "in water"
        /*if (rb.position.y > 0)
        {
            rb.AddForce(Physics.gravity);
            rb.drag = 0;
        }
        else
        {
            rb.AddForce(-1 * Physics.gravity);
            rb.drag = 5;
        }*/
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg - 90f;
    }

    //Physics helper script gotten from youtube which just makes setting speed and acceleration easier
    public static void ApplyForceToReachVelocity(Rigidbody rigidbody, Vector3 velocity, float force = 1, ForceMode mode = ForceMode.Force)
    {
        if (force == 0 || velocity.magnitude == 0)
            return;

        velocity = velocity + velocity.normalized * 0.2f * rigidbody.drag;

        //force = 1 => need 1 s to reach velocity (if mass is 1) => force can be max 1 / Time.fixedDeltaTime
        force = Mathf.Clamp(force, -rigidbody.mass / Time.fixedDeltaTime, rigidbody.mass / Time.fixedDeltaTime);

        //dot product is a projection from rhs to lhs with a length of result / lhs.magnitude https://www.youtube.com/watch?v=h0NJK4mEIJU
        if (rigidbody.velocity.magnitude == 0)
        {
            rigidbody.AddForce(velocity * force, mode);
        }
        else
        {
            var velocityProjectedToTarget = (velocity.normalized * Vector3.Dot(velocity, rigidbody.velocity) / velocity.magnitude);
            rigidbody.AddForce((velocity - velocityProjectedToTarget) * force, mode);
        }
    }
}