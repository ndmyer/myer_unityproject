﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;

    public float bulletForce = 20f;
    public float fireRate = 10f;

    private float nextTimeToFire = 0f;


    void Update()
    {
        if (Input.GetKey("space") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
    }

    void Shoot()
    {
        float bulletSpread = Random.Range(-100, 100);

        Vector3 spread = new Vector3(bulletSpread, 0.0f, 0.0f);

        Quaternion rotation = Quaternion.Euler(spread) * firePoint.rotation;

        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
    }
}
